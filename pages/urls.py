from django.urls import path

from pages.views import homePageView, HomePageView, AboutPageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('about/', AboutPageView.as_view(), name='about'),
    path('deff', homePageView, name='home2')
]